﻿
Option Compare Binary
Option Infer On
Option Strict On
Option Explicit On

Imports LaminateInventoryManagementRIA
Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports System.Data
Imports System.Linq
Imports System.ServiceModel.DomainServices.EntityFramework
Imports System.ServiceModel.DomainServices.Hosting
Imports System.ServiceModel.DomainServices.Server


'Implements application logic using the CCTERP2010Entities context.
' TODO: Add your application logic to these methods or in additional methods.
' TODO: Wire up authentication (Windows/ASP.NET Forms) and uncomment the following to disable anonymous access
' Also consider adding roles to restrict access as appropriate.
'<RequiresAuthentication> _
<EnableClientAccess()>  _
Public Class LaminateInventoryService
    Inherits LinqToEntitiesDomainService(Of CCTERP2010Entities)
    
    'TODO:
    ' Consider constraining the results of your query method.  If you need additional input you can
    ' add parameters to this method or create additional query methods with different names.
    'To support paging you will need to add ordering to the 'AppMaterialsLaminateBrands' query.
    Public Function GetAppMaterialsLaminateBrands() As IQueryable(Of AppMaterialsLaminateBrand)
        Return Me.ObjectContext.AppMaterialsLaminateBrands
    End Function
    
    'TODO:
    ' Consider constraining the results of your query method.  If you need additional input you can
    ' add parameters to this method or create additional query methods with different names.
    'To support paging you will need to add ordering to the 'AppMaterialsLaminateColors' query.
    Public Function GetAppMaterialsLaminateColors() As IQueryable(Of AppMaterialsLaminateColor)
        Return Me.ObjectContext.AppMaterialsLaminateColors
    End Function
    
    'TODO:
    ' Consider constraining the results of your query method.  If you need additional input you can
    ' add parameters to this method or create additional query methods with different names.
    'To support paging you will need to add ordering to the 'AppMaterialsLaminateFinishes' query.
    Public Function GetAppMaterialsLaminateFinishes() As IQueryable(Of AppMaterialsLaminateFinish)
        Return Me.ObjectContext.AppMaterialsLaminateFinishes
    End Function
    
    'TODO:
    ' Consider constraining the results of your query method.  If you need additional input you can
    ' add parameters to this method or create additional query methods with different names.
    'To support paging you will need to add ordering to the 'AppMaterialsLaminateGrades' query.
    Public Function GetAppMaterialsLaminateGrades() As IQueryable(Of AppMaterialsLaminateGrade)
        Return Me.ObjectContext.AppMaterialsLaminateGrades
    End Function
    
    'TODO:
    ' Consider constraining the results of your query method.  If you need additional input you can
    ' add parameters to this method or create additional query methods with different names.
    'To support paging you will need to add ordering to the 'AppMaterialsLaminateSizes' query.
    Public Function GetAppMaterialsLaminateSizes() As IQueryable(Of AppMaterialsLaminateSize)
        Return Me.ObjectContext.AppMaterialsLaminateSizes
    End Function
    
    'TODO:
    ' Consider constraining the results of your query method.  If you need additional input you can
    ' add parameters to this method or create additional query methods with different names.
    'To support paging you will need to add ordering to the 'InventoryAdjustmentComments' query.
    Public Function GetInventoryAdjustmentComments() As IQueryable(Of InventoryAdjustmentComment)
        Return Me.ObjectContext.InventoryAdjustmentComments
    End Function
    
    Public Sub InsertInventoryAdjustmentComment(ByVal inventoryAdjustmentComment As InventoryAdjustmentComment)
        If ((inventoryAdjustmentComment.EntityState = EntityState.Detached)  _
                    = false) Then
            Me.ObjectContext.ObjectStateManager.ChangeObjectState(inventoryAdjustmentComment, EntityState.Added)
        Else
            Me.ObjectContext.InventoryAdjustmentComments.AddObject(inventoryAdjustmentComment)
        End If
    End Sub
    
    Public Sub UpdateInventoryAdjustmentComment(ByVal currentInventoryAdjustmentComment As InventoryAdjustmentComment)
        Me.ObjectContext.InventoryAdjustmentComments.AttachAsModified(currentInventoryAdjustmentComment, Me.ChangeSet.GetOriginal(currentInventoryAdjustmentComment))
    End Sub
    
    Public Sub DeleteInventoryAdjustmentComment(ByVal inventoryAdjustmentComment As InventoryAdjustmentComment)
        If (inventoryAdjustmentComment.EntityState = EntityState.Detached) Then
            Me.ObjectContext.InventoryAdjustmentComments.Attach(inventoryAdjustmentComment)
        End If
        Me.ObjectContext.InventoryAdjustmentComments.DeleteObject(inventoryAdjustmentComment)
    End Sub
    
    'TODO:
    ' Consider constraining the results of your query method.  If you need additional input you can
    ' add parameters to this method or create additional query methods with different names.
    'To support paging you will need to add ordering to the 'LaminateInventories' query.
    Public Function GetLaminateInventories() As IQueryable(Of LaminateInventory)
        Return Me.ObjectContext.LaminateInventories.OrderBy(Function(e) e.laminateBrand)
    End Function
    
    Public Function GetFilteredLaminateInventories(ByVal Brand As String) As IQueryable(Of LaminateInventory)
        Dim result = Me.ObjectContext.LaminateInventories.Where(Function(e) e.laminateBrand = Brand).OrderBy(Function(e) e.laminateBrand)
        'Return result.Take(10)
        Return result.Skip(20).Take(10)
    End Function

    Public Sub InsertLaminateInventory(ByVal laminateInventory As LaminateInventory)
        If ((laminateInventory.EntityState = EntityState.Detached) _
                    = False) Then
            Me.ObjectContext.ObjectStateManager.ChangeObjectState(laminateInventory, EntityState.Added)
        Else
            Me.ObjectContext.LaminateInventories.AddObject(laminateInventory)
        End If
    End Sub
    
    Public Sub UpdateLaminateInventory(ByVal currentLaminateInventory As LaminateInventory)
        Me.ObjectContext.LaminateInventories.AttachAsModified(currentLaminateInventory, Me.ChangeSet.GetOriginal(currentLaminateInventory))
    End Sub
    
    Public Sub DeleteLaminateInventory(ByVal laminateInventory As LaminateInventory)
        If (laminateInventory.EntityState = EntityState.Detached) Then
            Me.ObjectContext.LaminateInventories.Attach(laminateInventory)
        End If
        Me.ObjectContext.LaminateInventories.DeleteObject(laminateInventory)
    End Sub
    
    'TODO:
    ' Consider constraining the results of your query method.  If you need additional input you can
    ' add parameters to this method or create additional query methods with different names.
    'To support paging you will need to add ordering to the 'LaminateTransactions' query.
    Public Function GetLaminateTransactions() As IQueryable(Of LaminateTransaction)
        Return Me.ObjectContext.LaminateTransactions
    End Function

End Class

