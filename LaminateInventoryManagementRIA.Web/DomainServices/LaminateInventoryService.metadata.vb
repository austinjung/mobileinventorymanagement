﻿
Option Compare Binary
Option Infer On
Option Strict On
Option Explicit On

Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports System.Linq
Imports System.ServiceModel.DomainServices.Hosting
Imports System.ServiceModel.DomainServices.Server


'The MetadataTypeAttribute identifies AppMaterialsLaminateBrandMetadata as the class
' that carries additional metadata for the AppMaterialsLaminateBrand class.
<MetadataTypeAttribute(GetType(AppMaterialsLaminateBrand.AppMaterialsLaminateBrandMetadata))>  _
Partial Public Class AppMaterialsLaminateBrand
    
    'This class allows you to attach custom attributes to properties
    ' of the AppMaterialsLaminateBrand class.
    '
    'For example, the following marks the Xyz property as a
    ' required property and specifies the format for valid values:
    '    <Required()>
    '    <RegularExpression("[A-Z][A-Za-z0-9]*")>
    '    <StringLength(32)>
    '    Public Property Xyz As String
    Friend NotInheritable Class AppMaterialsLaminateBrandMetadata
        
        'Metadata classes are not meant to be instantiated.
        Private Sub New()
            MyBase.New
        End Sub
        
        Public Property amlbCode As String
        
        Public Property amlbCreated As DateTime
        
        Public Property amlbDescription As String
        
        Public Property amlbID As Integer
        
        Public Property amlbIsAvailable As Boolean
        
        Public Property amlbModified As DateTime
        
        Public Property amlbNextSeqNumber As Integer
    End Class
End Class

'The MetadataTypeAttribute identifies AppMaterialsLaminateColorMetadata as the class
' that carries additional metadata for the AppMaterialsLaminateColor class.
<MetadataTypeAttribute(GetType(AppMaterialsLaminateColor.AppMaterialsLaminateColorMetadata))>  _
Partial Public Class AppMaterialsLaminateColor
    
    'This class allows you to attach custom attributes to properties
    ' of the AppMaterialsLaminateColor class.
    '
    'For example, the following marks the Xyz property as a
    ' required property and specifies the format for valid values:
    '    <Required()>
    '    <RegularExpression("[A-Z][A-Za-z0-9]*")>
    '    <StringLength(32)>
    '    Public Property Xyz As String
    Friend NotInheritable Class AppMaterialsLaminateColorMetadata
        
        'Metadata classes are not meant to be instantiated.
        Private Sub New()
            MyBase.New
        End Sub
        
        Public Property amlbID As Integer
        
        Public Property amlcCode As String
        
        Public Property amlcCreated As DateTime
        
        Public Property amlcDescription As String
        
        Public Property amlcID As Integer
        
        Public Property amlcIsAvailable As Boolean
        
        Public Property amlcModified As DateTime
    End Class
End Class

'The MetadataTypeAttribute identifies AppMaterialsLaminateFinishMetadata as the class
' that carries additional metadata for the AppMaterialsLaminateFinish class.
<MetadataTypeAttribute(GetType(AppMaterialsLaminateFinish.AppMaterialsLaminateFinishMetadata))>  _
Partial Public Class AppMaterialsLaminateFinish
    
    'This class allows you to attach custom attributes to properties
    ' of the AppMaterialsLaminateFinish class.
    '
    'For example, the following marks the Xyz property as a
    ' required property and specifies the format for valid values:
    '    <Required()>
    '    <RegularExpression("[A-Z][A-Za-z0-9]*")>
    '    <StringLength(32)>
    '    Public Property Xyz As String
    Friend NotInheritable Class AppMaterialsLaminateFinishMetadata
        
        'Metadata classes are not meant to be instantiated.
        Private Sub New()
            MyBase.New
        End Sub
        
        Public Property amlbID As Integer
        
        Public Property amlfCode As String
        
        Public Property amlfCreated As DateTime
        
        Public Property amlfDescription As String
        
        Public Property amlfID As Integer
        
        Public Property amlfIsAvailable As Boolean
        
        Public Property amlfModified As DateTime
    End Class
End Class

'The MetadataTypeAttribute identifies AppMaterialsLaminateGradeMetadata as the class
' that carries additional metadata for the AppMaterialsLaminateGrade class.
<MetadataTypeAttribute(GetType(AppMaterialsLaminateGrade.AppMaterialsLaminateGradeMetadata))>  _
Partial Public Class AppMaterialsLaminateGrade
    
    'This class allows you to attach custom attributes to properties
    ' of the AppMaterialsLaminateGrade class.
    '
    'For example, the following marks the Xyz property as a
    ' required property and specifies the format for valid values:
    '    <Required()>
    '    <RegularExpression("[A-Z][A-Za-z0-9]*")>
    '    <StringLength(32)>
    '    Public Property Xyz As String
    Friend NotInheritable Class AppMaterialsLaminateGradeMetadata
        
        'Metadata classes are not meant to be instantiated.
        Private Sub New()
            MyBase.New
        End Sub
        
        Public Property amlgCode As String
        
        Public Property amlgCreated As DateTime
        
        Public Property amlgDescription As String
        
        Public Property amlgID As Integer
        
        Public Property amlgIsAvailable As Boolean
        
        Public Property amlgModified As DateTime
    End Class
End Class

'The MetadataTypeAttribute identifies AppMaterialsLaminateSizeMetadata as the class
' that carries additional metadata for the AppMaterialsLaminateSize class.
<MetadataTypeAttribute(GetType(AppMaterialsLaminateSize.AppMaterialsLaminateSizeMetadata))>  _
Partial Public Class AppMaterialsLaminateSize
    
    'This class allows you to attach custom attributes to properties
    ' of the AppMaterialsLaminateSize class.
    '
    'For example, the following marks the Xyz property as a
    ' required property and specifies the format for valid values:
    '    <Required()>
    '    <RegularExpression("[A-Z][A-Za-z0-9]*")>
    '    <StringLength(32)>
    '    Public Property Xyz As String
    Friend NotInheritable Class AppMaterialsLaminateSizeMetadata
        
        'Metadata classes are not meant to be instantiated.
        Private Sub New()
            MyBase.New
        End Sub
        
        Public Property amlsCode As String
        
        Public Property amlsCreated As DateTime
        
        Public Property amlsDescription As String
        
        Public Property amlsID As Integer
        
        Public Property amlsIsAvailable As Boolean
        
        Public Property amlsLength As Integer
        
        Public Property amlsModified As DateTime
        
        Public Property amlsUOM As String
        
        Public Property amlsWidth As Integer
    End Class
End Class

'The MetadataTypeAttribute identifies InventoryAdjustmentCommentMetadata as the class
' that carries additional metadata for the InventoryAdjustmentComment class.
<MetadataTypeAttribute(GetType(InventoryAdjustmentComment.InventoryAdjustmentCommentMetadata))>  _
Partial Public Class InventoryAdjustmentComment
    
    'This class allows you to attach custom attributes to properties
    ' of the InventoryAdjustmentComment class.
    '
    'For example, the following marks the Xyz property as a
    ' required property and specifies the format for valid values:
    '    <Required()>
    '    <RegularExpression("[A-Z][A-Za-z0-9]*")>
    '    <StringLength(32)>
    '    Public Property Xyz As String
    Friend NotInheritable Class InventoryAdjustmentCommentMetadata
        
        'Metadata classes are not meant to be instantiated.
        Private Sub New()
            MyBase.New
        End Sub
        
        Public Property adjustmentComment As String
        
        Public Property adjustmentType As String
        
        Public Property comment_id As Integer
        
        Public Property created As DateTime
        
        Public Property isValid As Boolean
        
        Public Property modified As DateTime
        
        Public Property targetInventory As String
    End Class
End Class

'The MetadataTypeAttribute identifies LaminateInventoryMetadata as the class
' that carries additional metadata for the LaminateInventory class.
<MetadataTypeAttribute(GetType(LaminateInventory.LaminateInventoryMetadata))>  _
Partial Public Class LaminateInventory
    
    'This class allows you to attach custom attributes to properties
    ' of the LaminateInventory class.
    '
    'For example, the following marks the Xyz property as a
    ' required property and specifies the format for valid values:
    '    <Required()>
    '    <RegularExpression("[A-Z][A-Za-z0-9]*")>
    '    <StringLength(32)>
    '    Public Property Xyz As String
    Friend NotInheritable Class LaminateInventoryMetadata
        
        'Metadata classes are not meant to be instantiated.
        Private Sub New()
            MyBase.New
        End Sub
        
        Public Property created As DateTime
        
        Public Property dateLastReceived As Nullable(Of DateTime)
        
        Public Property dateLastUsed As Nullable(Of DateTime)
        
        Public Property laminateBrand As String
        
        Public Property laminateBrandDescription As String
        
        Public Property laminateColor As String
        
        Public Property laminateColorDescription As String
        
        Public Property laminateFinish As String
        
        Public Property laminateFinishDescription As String
        
        Public Property laminateGrade As String
        
        Public Property laminateGradeDescription As String
        
        Public Property laminateSize As String
        
        Public Property laminateSizeDescription As String
        
        Public Property modified As DateTime
        
        Public Property qtyOnHand As Double
    End Class
End Class

'The MetadataTypeAttribute identifies LaminateTransactionMetadata as the class
' that carries additional metadata for the LaminateTransaction class.
<MetadataTypeAttribute(GetType(LaminateTransaction.LaminateTransactionMetadata))>  _
Partial Public Class LaminateTransaction
    
    'This class allows you to attach custom attributes to properties
    ' of the LaminateTransaction class.
    '
    'For example, the following marks the Xyz property as a
    ' required property and specifies the format for valid values:
    '    <Required()>
    '    <RegularExpression("[A-Z][A-Za-z0-9]*")>
    '    <StringLength(32)>
    '    Public Property Xyz As String
    Friend NotInheritable Class LaminateTransactionMetadata
        
        'Metadata classes are not meant to be instantiated.
        Private Sub New()
            MyBase.New
        End Sub
        
        Public Property laminateBrand As String
        
        Public Property laminateBrandDescription As String
        
        Public Property laminateColor As String
        
        Public Property laminateColorDescription As String
        
        Public Property laminateFinish As String
        
        Public Property laminateFinishDescription As String
        
        Public Property laminateGrade As String
        
        Public Property laminateGradeDescription As String
        
        Public Property laminateSize As String
        
        Public Property laminateSizeDescription As String
        
        Public Property lamTransID As Integer
        
        Public Property poReference As Nullable(Of Integer)
        
        Public Property productionReference As Nullable(Of Integer)
        
        Public Property transactionComment As String
        
        Public Property transactionQty As Double
        
        Public Property transactionTime As DateTime
        
        Public Property transactionType As String
    End Class
End Class

