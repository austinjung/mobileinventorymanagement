﻿Imports System
Imports System.Reflection
Imports System.Resources
Imports System.Runtime.CompilerServices
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.
<Assembly: AssemblyTitle("LaminateInventoryManagementRIA.Web")>
<Assembly: AssemblyDescription("")>
<Assembly: AssemblyConfiguration("")>
<Assembly: AssemblyCompany("columbiacabinets")> 
<Assembly: AssemblyProduct("LaminateInventoryManagementRIA.Web")>
<Assembly: AssemblyCopyright("Copyright © columbiacabinets 2011")> 
<Assembly: AssemblyTrademark("")>
<Assembly: AssemblyCulture("")>
<Assembly: CLSCompliant(True)>

' Setting ComVisible to false makes the types in this assembly not visible 
' to COM components. If you need to access a type in this assembly from 
' COM, set the ComVisible attribute to true on that type.
<Assembly: ComVisible(False)>

' The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("6a2c977d-89d0-4a09-889d-9edd2d47281c")>

<Assembly: NeutralResourcesLanguageAttribute("en-us")>

' Version information for an assembly consists of the following four values:
'
' Major Version
' Minor Version 
' Build Number
' Revision
'
' You can specify all the values or you can default the Revision and Build Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")>
<Assembly: AssemblyVersion("1.0.0.0")>
<Assembly: AssemblyFileVersion("1.0.0.0")>